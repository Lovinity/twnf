import './lib/setup';
import { LogLevel, SapphireClient } from '@sapphire/framework';
import { Settings } from 'luxon';

// Garbage collector; needed for canvas
var v8 = require("v8");
var vm = require('vm');
v8.setFlagsFromString('--expose-gc');
global.gc = vm.runInNewContext('gc');

const client = new SapphireClient({
	defaultPrefix: 'twnf!',
	regexPrefix: /^(hey +)?twnf[,! ]/i,
	caseInsensitiveCommands: true,
	logger: {
		level: LogLevel.Debug
	},
	shards: 'auto',
	intents: ["GUILDS", "GUILD_MESSAGES"]
});

const main = async () => {
	try {
		client.logger.info('Logging in');
		await client.login();
		client.logger.info('logged in');
	} catch (error) {
		client.logger.fatal(error);
		client.destroy();
		process.exit(1);
	}
};

// Default to America / New York for the time zone of Wright State
Settings.defaultZone = 'America/New_York';

main();
