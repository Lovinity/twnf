import { ApplyOptions } from '@sapphire/decorators';
import { Listener, ListenerOptions } from '@sapphire/framework';
import { AsyncQueue } from '@sapphire/async-queue';
import { Guild, TextChannel, NewsChannel, Message, MessageEmbed } from 'discord.js';

import { weatherSettings } from '../config';
import type { WeatherSource, AlertsResponseModified, WeatherAlertDbRecords, WeatherAlertDbRecord, WeatherAlertDbRecordAlert } from '../lib/types';
import {
	weatherAlertCertaintyExplanation,
	weatherAlertResponseTypeExplanation,
	weatherAlertSeverityColors,
	weatherAlertSeverityExplanation,
	weatherAlertSeverityIcons,
	weatherAlertUrgencyExplanation
} from '../lib/constants';

import { Client } from 'weathered';
import * as cron from 'node-cron';
import { gray, green, magentaBright, yellow, redBright } from 'colorette';
import { DateTime } from 'luxon';

import { JsonDB, Config } from 'node-json-db';

@ApplyOptions<ListenerOptions>({
	once: true,
	event: 'ready'
})
export class ReadyWeatherAlertsEvent extends Listener {
	private queue = new AsyncQueue();
	private messageQueue = new AsyncQueue();

	private api = new Client();

	private db = new Map<string, JsonDB>();

	private activeIDs: string[] = [];
	private cleanup: number = 0;

	public run() {
		this.container.logger.info(`${gray(`ReadyWeatherAlertsEvent:`)} Preparing...`);

		// Initialize weather alerts database for every weather source
		// TODO: This works around several node-json-db bugs.
		weatherSettings.weather.forEach((weather) => {
			this.db.set(weather.key, new JsonDB(new Config(`weatheralerts_${weather.key}`, true, false, '|')));
		});

		// Schedule a "cron job" to run the weather operations
		cron.schedule(weatherSettings.cronTab, () => {
			this.container.logger.info(`${gray(`ReadyWeatherAlertsEvent CRON:`)} Starting.`);

			// Reset trackers
			this.activeIDs = [];
			this.cleanup = 0;

			// Queue each weather operation from the config
			weatherSettings.weather.forEach((weather) => {
				this.runWeatherAlerts(weather);
			});

			this.queueCleanup();

			this.container.logger.info(`${gray(`ReadyWeatherAlertsEvent CRON:`)} ${green(`queued`)}`);
		});

		this.container.logger.info(`${gray(`ReadyWeatherAlertsEvent:`)} ${green(`READY`)}`);
	}

	/**
	 * Add to queue to get weather alerts for a source.
	 * @param weather The configured weather source to use
	 */
	private async runWeatherAlerts(weather: WeatherSource) {
		// Wait for the queue to be ready to process the next operation.
		await this.queue.wait();
		this.container.logger.info(
			`${gray(`ReadyWeatherAlertsEvent.runWeatherAlerts:`)} ${magentaBright(`Running NWS alerts check on ${weather.name}.`)}`
		);

		try {
			// Get active weather alerts
			let weatherAlerts = (await this.api.getAlerts(true, {
				latitude: weather.lat,
				longitude: weather.long
			})) as unknown as AlertsResponseModified; // weathered does not have the correct type definitions
			if (!weatherAlerts || !weatherAlerts.features) return;

			this.cleanup++; // At this point, consider the fetch successful
			this.container.logger.info(
				`${gray(`ReadyWeatherAlertsEvent.runWeatherAlerts[${weather.key}]:`)} There are ${magentaBright(
					weatherAlerts.features.length
				)} active alerts.`
			);

			if (!weatherAlerts.features.length) return; // Do not continue if there are no active alerts

			// Map weather alert IDs
			weatherAlerts.features.forEach((feature) => this.activeIDs.push(feature.id));

			// Process each weather alert
			weatherAlerts.features.forEach(async (feature) => {
				// Build data
				let alertInfo: WeatherAlertDbRecordAlert = {
					event: feature.properties.event,
					headline: feature.properties.headline,
					description: feature.properties.description,
					instruction: feature.properties.instruction,
					effective: feature.properties.effective,
					expires: feature.properties.expires,
					onset: feature.properties.onset,
					ends: feature.properties.ends,
					urgency: feature.properties.urgency,
					severity: feature.properties.severity,
					certainty: feature.properties.certainty,
					response: feature.properties.response,
					tornadoDamageThreat: feature.properties.parameters.tornadoDamageThreat
						? feature.properties.parameters.tornadoDamageThreat[0]
						: undefined,
					tornadoDetection: feature.properties.parameters.tornadoDetection ? feature.properties.parameters.tornadoDetection[0] : undefined,
					windGust: feature.properties.parameters.windGust ? feature.properties.parameters.windGust[0] : undefined,
					hailSize: feature.properties.parameters.hailSize ? feature.properties.parameters.hailSize[0] : undefined,
					eventEndingTime: feature.properties.parameters.eventEndingTime ? feature.properties.parameters.eventEndingTime[0] : undefined
				};

				// Determine tornado emergencies and particularly dangerous alerts
				let additionalWarning: string = '';

				// Tornadoes
				if (feature.properties.parameters.tornadoDamageThreat && feature.properties.parameters.tornadoDamageThreat[0]) {
					if (feature.properties.parameters.tornadoDamageThreat[0] === 'CATASTROPHIC') {
						additionalWarning +=
							'\n\n' +
							`@everyone ‼️ **__TORNADO EMERGENCY!!!__ A __VERY LARGE AND DESTRUCTIVE tornado__ is reported on the ground RIGHT NOW!** ‼️ __CATASTROPHIC DAMAGE__ is occurring! The threat to life and property is __EXTREME__! **__ACT QUICKLY TO PROTECT YOUR LIFE!!! SEEK SAFE SHELTER IMMEDIATELY!!! DO NOT IGNORE THIS WARNING!!!__**`;
					} else {
						additionalWarning +=
							'\n\n' +
							`@everyone ⚠️ **A __PARTICULARLY DANGEROUS (PDS) tornado__ is reported on the ground RIGHT NOW!** ⚠️ __Considerable damage__ is occurring! The threat to life and property is severe! **__SEEK SAFE SHELTER IMMEDIATELY! DO NOT IGNORE THIS WARNING!__**`;
					}
				} else if (feature.properties.parameters.tornadoDetection && feature.properties.parameters.tornadoDetection[0]) {
					if (feature.properties.parameters.tornadoDetection[0] === 'OBSERVED') {
						additionalWarning +=
							'\n\n' +
							`@everyone ⚠️ **A tornado is reported on the ground RIGHT NOW!** ⚠️ Damage is likely! The threat to life and property is significant! **__SEEK SAFE SHELTER IMMEDIATELY! DO NOT IGNORE THIS WARNING!__**`;
					}
				}

				// Severe Thunderstorms etc
				if (feature.properties.parameters.windGust && feature.properties.parameters.windGust[0]) {
					if (parseInt(feature.properties.parameters.windGust[0]) >= 75) {
						additionalWarning +=
							'\n\n' +
							`@here ⚠️ **This is a PARTICULARLY DANGEROUS situation (PDS)!** ⚠️ This severe thunderstorm is capable of producing ${parseInt(
								feature.properties.parameters.windGust[0]
							)} MPH wind gusts. Significant damage is likely! You will get injured if you remain outside! __**Seek safe shelter immediately!**__`;
					}
				}
				if (feature.properties.parameters.hailSize && feature.properties.parameters.hailSize[0]) {
					if (parseFloat(feature.properties.parameters.hailSize[0]) >= 2) {
						additionalWarning +=
							'\n\n' +
							`@here ⚠️ **This is a PARTICULARLY DANGEROUS situation (PDS)!** ⚠️ This severe thunderstorm is capable of producing ${parseFloat(
								feature.properties.parameters.hailSize[0]
							)} inch hail. Significant damage is likely! You will get injured if you remain outside! __**Seek safe shelter immediately!**__`;
					}
				}

				// Build discord embed and message
				let message: string = `${weatherAlertSeverityIcons.get(feature.properties.severity) || '⚫'} **__${
					feature.properties.headline || feature.properties.event
				}__** ${weatherAlertSeverityIcons.get(feature.properties.severity) || '⚫'}
${additionalWarning}

Please activate links / embeds to see the alert details below.`;

				let fullDescription: string = feature.properties.description + '\n\n' + feature.properties.instruction || '';

				let embed: MessageEmbed = new MessageEmbed()
					.setTitle(
						`${weatherAlertSeverityIcons.get(feature.properties.severity) || '⚫'} ${
							feature.properties.headline || feature.properties.event
						} ${weatherAlertSeverityIcons.get(feature.properties.severity) || '⚫'}`
					)
					.setDescription(
						fullDescription.length < 4000
							? fullDescription
							: `Description too long; please click the embed title to view alert information.`
					)
					.setURL(feature.id)
					.setColor(weatherAlertSeverityColors.get(feature.properties.severity) || '#000000')
					.addFields([
						{
							name: 'Alert In Effect',
							value: `${DateTime.fromISO(feature.properties.onset || feature.properties.effective).toLocaleString(
								DateTime.DATETIME_SHORT
							)} - ${DateTime.fromISO(feature.properties.ends || feature.properties.expires).toLocaleString(DateTime.DATETIME_SHORT)}${
								feature.properties.parameters.eventEndingTime && feature.properties.parameters.eventEndingTime[0]
									? `(Hazard is expected until ${DateTime.fromISO(feature.properties.parameters.eventEndingTime[0]).toLocaleString(
											DateTime.DATETIME_SHORT
									  )})`
									: ``
							}`
						},
						{
							name: 'Severity',
							value: `${feature.properties.severity} (${
								weatherAlertSeverityExplanation.get(feature.properties.severity) || 'Unknown description'
							})`
						},
						{
							name: 'Urgency',
							value: `${feature.properties.urgency} (${
								weatherAlertUrgencyExplanation.get(feature.properties.urgency) || 'Unknown description'
							})`
						},
						{
							name: 'Certainty',
							value: `${feature.properties.certainty} (${
								weatherAlertCertaintyExplanation.get(feature.properties.certainty) || 'Unknown description'
							})`
						},
						{
							name: 'Response',
							value: `${feature.properties.response || 'None'} (${
								weatherAlertResponseTypeExplanation.get(feature.properties.response || 'None') || 'Unknown description'
							})`
						}
					]);

				// Queue operation
				this.processWeatherAlert(`Alert ${feature.id}`, async () => {
					let db = this.db.get(weather.key);
					if (!db) throw new Error('Weather alerts database error!');

					let dbRecord: WeatherAlertDbRecord | undefined;

					// Get or create the record in the db
					try {
						dbRecord = await db.getObject<WeatherAlertDbRecord>(`|${feature.id}`);
						if (!dbRecord) {
							throw new Error(`Record does not exist`);
						}
					} catch {
						// Record did not exist. Create it along with a Discord message
						this.container.logger.info(
							`${gray(`ReadyWeatherAlertsEvent.processWeatherAlert.process:`)} ${magentaBright(
								`Alert ${feature.id}`
							)}: Creating a Discord message.`
						);
						let guild = await this.container.client.guilds.fetch(weather.guild);
						if (guild instanceof Guild) {
							let channel = await guild.channels.fetch(weather.channel);
							if (channel instanceof TextChannel || channel instanceof NewsChannel) {
								let dcMessage = await (channel as TextChannel).send({ content: message, embeds: [embed] });
								if (!dcMessage) throw new Error(`Error sending alerts message.`);

								db.push(`|${feature.id}`, {
									discordMessageID: dcMessage.id,
									discordChannelID: weather.channel,
									discordGuildID: weather.guild,
									discordMessage: message,
									alert: alertInfo
								} as WeatherAlertDbRecord);
							}
						}
					}

					if (dbRecord) {
						// Check if the details changed and we need to update the Discord message.
						let needsUpdated: boolean = false;
						for (let key in alertInfo) {
							if (
								Object.prototype.hasOwnProperty.call(alertInfo, key) &&
								alertInfo[key as keyof WeatherAlertDbRecordAlert] !== dbRecord.alert[key as keyof WeatherAlertDbRecordAlert]
							)
								needsUpdated = true;
						}

						if (needsUpdated) {
							this.container.logger.info(
								`${gray(`ReadyWeatherAlertsEvent.processWeatherAlert.process:`)} ${magentaBright(
									`Alert ${feature.id}`
								)}: Updating Discord message.`
							);
							let guild = await this.container.client.guilds.fetch(weather.guild);
							if (guild instanceof Guild) {
								let channel = await guild.channels.fetch(weather.channel);
								if (channel instanceof TextChannel || channel instanceof NewsChannel) {
									let dcMessage = await (channel as TextChannel).messages.fetch(dbRecord.discordMessageID);
									if (dcMessage instanceof Message) {
										await dcMessage.edit({ content: message, embeds: [embed] });

										db.push(`|${feature.id}`, {
											discordMessageID: dcMessage.id,
											discordChannelID: weather.channel,
											discordGuildID: weather.guild,
											discordMessage: message,
											alert: alertInfo
										} as WeatherAlertDbRecord);
									}
								}
							}
						}
					} else {
						this.container.logger.info(
							`${gray(`ReadyWeatherAlertsEvent.processWeatherAlert.process:`)} ${magentaBright(`Alert ${feature.id}`)}: No changes.`
						);
					}
				});
			});
		} catch (error) {
			this.container.client.logger.error(error);
		} finally {
			setTimeout(() => this.queue.shift(), 15000);
		}
	}

	private async processWeatherAlert(name: string, fn: () => Promise<void>) {
		// Wait for the queue to be ready to process the next operation.
		await this.messageQueue.wait();
		this.container.logger.info(`${gray(`ReadyWeatherAlertsEvent.processWeatherAlert:`)} Running operation ${magentaBright(name)}.`);
		try {
			await fn();
		} catch (error) {
			this.container.client.logger.error(error);
		} finally {
			this.container.logger.info(
				`${gray(`ReadyWeatherAlertsEvent.processWeatherAlert:`)} Operation ${magentaBright(name)} ${green(`DONE`)}. ${yellow(
					this.messageQueue.remaining - 1
				)} message operations in queue.`
			);
			setTimeout(() => this.messageQueue.shift(), 3000);
		}
	}

	/**
	 * Queue a cleanup procedure to remove messages / db records whose incidents no longer exist.
	 */
	private async queueCleanup() {
		// Queue delete / cleanup once all traffic items have been processed
		await this.queue.wait();

		// Skip cleanup if a source malfunctioned
		if (this.cleanup < weatherSettings.weather.length) {
			this.container.logger.warn(
				`${gray(
					`ReadyWeatherAlertsEvent.queueCleanup:`
				)} One or more weather alert sources malfunctioned / did not return data. Cleanup was skipped for now.`
			);

			this.queue.shift();
			return;
		}

		this.container.logger.info(`${gray(`ReadyWeatherAlertsEvent.queueCleanup:`)} ${yellow(`started`)}.`);

		for (const db of this.db.values()) {
			// Iterate over each alert in the database
			let dbRecords = await db.getObject<WeatherAlertDbRecords>('|');
			for (let key in dbRecords) {
				if (!Object.prototype.hasOwnProperty.call(dbRecords, key)) continue;

				// No longer in effect? Queue it for deletion.
				if (this.activeIDs.indexOf(key) === -1) {
					this.container.logger.info(
						`${gray(`ReadyWeatherAlertsEvent.queueCleanup:`)} ${magentaBright(key)} no longer active and queued for deletion.`
					);

					this.processWeatherAlert(`Delete: ${key}`, async () => {
						// Grab the record from the database
						let dbRecord: WeatherAlertDbRecord | undefined;
						try {
							dbRecord = await db.getObject<WeatherAlertDbRecord>(`|${key}`);
						} catch {
							// Ignore errors
						}
						if (!dbRecord) return; // Does not exist? We don't have to do anything. Exit.

						// Since we are deleting the record first, we have to make a copy of the snowflake.
						let dcMessageID = dbRecord.discordMessageID;
						let dcChannelID = dbRecord.discordChannelID;
						let dcGuildID = dbRecord.discordGuildID;

						// Delete the db record.
						db.delete(`|${key}`);
						this.container.logger.info(
							`${gray(`ReadyWeatherAlertsEvent.processWeatherAlert.cleanup:`)} ${redBright(`DELETED`)} db entry ${magentaBright(key)}.`
						);

						// Delete the Discord message
						let guild = await this.container.client.guilds.fetch(dcGuildID);
						if (guild instanceof Guild) {
							let channel = await guild.channels.fetch(dcChannelID);
							if (channel instanceof TextChannel || channel instanceof NewsChannel) {
								let dcMessage = await (channel as TextChannel).messages.fetch(dcMessageID);
								if (dcMessage instanceof Message) {
									await dcMessage.delete();
									this.container.logger.info(
										`${gray(`ReadyWeatherAlertsEvent.processWeatherAlert.cleanup:`)} ${redBright(
											`DELETED`
										)} discord message for db entry ${magentaBright(key)}.`
									);
								}
							}
						}
					});
				}
			}
		}

		this.container.logger.info(`${gray(`ReadyWeatherAlertsEvent.queueCleanup:`)} ${green(`PROCESSED`)}.`);
		this.queue.shift();
	}
}
