import { ApplyOptions } from '@sapphire/decorators';
import { Listener, ListenerOptions } from '@sapphire/framework';
import { gray, green, magentaBright } from 'colorette';

import * as cron from 'node-cron';
import { AsyncQueue } from '@sapphire/async-queue';
import OpenWeatherAPI from 'openweather-api-node';
import { MessageEmbed, Guild, TextChannel, NewsChannel, Message } from 'discord.js';
import type { WeatherSource } from '../lib/types';
import { weatherSettings } from '../config';
import { degToCard } from '../lib/utils';
import { weatherIcons, weatherIconCodes } from '../lib/constants';
import { DateTime } from 'luxon';

import type { MinutelyWeather, HourlyWeather, DailyWeather } from 'openweather-api-node';

@ApplyOptions<ListenerOptions>({
	once: true,
	event: 'ready'
})
export class ReadyWeatherEvent extends Listener {
	private queue = new AsyncQueue();

	private api = new OpenWeatherAPI({
		key: weatherSettings.openWeatherMap.apiKey,
		units: weatherSettings.openWeatherMap.units
	});

	public run() {
		/* 	Uncomment this, weatherSettings import, and the TextChannel type import when you need to re-generate 
			weather messages. Run the bot and immediately stop and update config.js with the new message IDs. 
			Then comment this back out.
		*/

		/*
			weatherSettings.weather.forEach(async (setting) => {
				let guild = await this.container.client.guilds.fetch(setting.guild);
				let channel = await guild.channels.fetch(setting.channel);
				await (channel as TextChannel).send(
					`This message contains current weather, forecast, and alerts for ${setting.name}. Please activate embeds / link previews / rich content to see it.`
				);
			});
		*/
		this.container.logger.info(`${gray(`ReadyWeatherEvent:`)} Preparing...`);

		// Schedule a "cron job" to run the weather operations
		cron.schedule(weatherSettings.cronTab, () => {
			this.container.logger.info(`${gray(`ReadyWeatherEvent CRON:`)} Starting.`);

			// Queue each weather operation from the config
			weatherSettings.weather.forEach((weather) => {
				this.runWeather(weather);
			});

			this.container.logger.info(`${gray(`ReadyWeatherEvent CRON:`)} ${green(`queued`)}`);
		});

		this.container.logger.info(`${gray(`ReadyWeatherEvent:`)} ${green(`READY`)}`);
	}

	/**
	 * Add a weather operation to the queue.
	 * @param {WeatherSource} weather A WeatherSource object from the config file
	 */
	private async runWeather(weather: WeatherSource) {
		// Wait for the queue to be ready to process the next operation.
		await this.queue.wait();
		this.container.logger.info(`${gray(`ReadyWeatherEvent.runWeather:`)} ${magentaBright(`Running OpenWeatherMap on ${weather.name}.`)}`);

		try {
			// Get weather for this location
			this.api.setLocationByCoordinates(weather.lat, weather.long);
			let response = await this.api.getEverything();

			// Prepare a message embed
			let embed = new MessageEmbed();
			embed.setTitle(`Weather for ${weather.name}`).setColor(weather.color).setTimestamp().setFooter({
				text: `Updates every 5 minutes from OpenWeatherMap`
			});

			// Current weather
			let current = response.current;
			if (current) {
				embed
					.setDescription(
						`${weatherIconCodes.get(current?.weather.conditionId)} Conditions: **${current?.weather.description}**
${weatherIcons.temperature} Temperature: **${current?.weather.temp.cur}°F** (feels like ${current?.weather.feelsLike.cur}°F)
${current?.weather.rain ? `${weatherIcons.rain} Rainfall Rate: **${current?.weather.rain} fl. mm/hr**` : ``}${
							current?.weather.snow
								? `
${weatherIcons.snow} Snowfall Rate: **${current?.weather.snow} fl. mm/hr**`
								: ``
						}
${weatherIcons.wind} Wind: from the **${degToCard(current?.weather.wind.deg || 0)}** at **${current?.weather.wind.speed} MPH** ${
							current?.weather.wind.gust ? `(Gusting to ${current?.weather.wind.gust} MPH)` : ``
						}
${weatherIcons.humidity} Relative Humidity: **${current?.weather.humidity}%** (dew point ${current?.weather.dewPoint}°F)
${weatherIcons.pressure} Barometric Pressure: **${current?.weather.pressure} mb**
${weatherIcons.visibility} Visibility: **${current?.weather.visibility / 1000} miles**
${weatherIcons.uvIndex} UV Index: **${current?.weather.uvi} / 11**`
					)
					.setThumbnail(current.weather.icon.url);
			} else {
				embed.setDescription(`:x: Current observations are unavailable at this time.`);
			}

			// Precip next hour
			let minutely = response.minutely as unknown as MinutelyWeather[];
			let counter = 0;
			let currentMax = 0;
			let fieldValue = `Now |`;
			if (minutely.length) {
				for (let minObj in minutely) {
					if (!Object.prototype.hasOwnProperty.call(minutely, minObj)) continue;

					// We are gathering the maximum precip rate in 5-minute intervals.
					// Then, when precip is predicted, display a number 1-9 indicating intensity.
					// Higher number = exponentially heavier precipitation.

					counter++;

					let precip = minutely[minObj].weather.rain;

					if (precip > currentMax) {
						currentMax = precip;
					}

					// Every 5th loop, tally a number based on max precip rate or display a - for no precip.
					if (counter % 5 === 0) {
						fieldValue += currentMax > 0 ? Math.ceil(Math.sqrt(currentMax)) : `-`;
						currentMax = 0;
					}
				}

				fieldValue += `| ${DateTime.now().plus({ hours: 1 }).toLocaleString(DateTime.TIME_SIMPLE)}`;
				embed.addFields([{ name: `Precip Outlook next hour (higher number = heavier precip)`, value: fieldValue }]);
			} else {
				embed.addFields([{ name: `Precip Outlook next hour (higher number = heavier precip)`, value: `Unavailable right now.` }]);
			}

			embed.addFields([{ name: `\u200b`, value: `\u200b` }]);

			// Weather Alerts
			let alerts = response.alerts;
			if (alerts?.length) {
				embed.addFields([
					{
						name: `Weather Alerts`,
						value: `${alerts
							.map(
								(alert) =>
									`🔴 **${alert.event}** from ${DateTime.fromSeconds(alert.start).toLocaleString(
										DateTime.DATETIME_SHORT
									)} to ${DateTime.fromSeconds(alert.end).toLocaleString(DateTime.DATETIME_SHORT)}`
							)
							.join('\n')}`
					}
				]);
			} else {
				embed.addFields([{ name: `Weather Alerts`, value: `There are no active alerts.` }]);
			}

			embed.addFields([{ name: `\u200b`, value: `\u200b` }]);

			// Hourly forecast
			let hourly = response.hourly as unknown as HourlyWeather[];
			if (hourly?.length) {
				let forecastFields: string[] = [];

				hourly.forEach((h, i) => {
					if (i < 1 || i > 24) return;
					let index = Math.floor((i - 1) / 6);
					if (typeof forecastFields[index] === 'string') {
						forecastFields[index] +=
							'\n' +
							`**${DateTime.fromJSDate(h.dt).toLocaleString(DateTime.TIME_SIMPLE)}**: ${weatherIconCodes.get(h.weather.conditionId)} (${h.weather.description}) ${
								h.weather.temp.cur
							}°F`;
					} else {
						forecastFields[index] = `**${DateTime.fromJSDate(h.dt).toLocaleString(DateTime.TIME_SIMPLE)}**: ${weatherIconCodes.get(
							h.weather.conditionId
						)} (${h.weather.description}) ${h.weather.temp.cur}°F`;
					}
				});

				forecastFields.forEach((field, index) => {
					embed.addFields([{ name: `Forecast: Hour ${index * 6 + 1} - ${index * 6 + 6}`, value: field }]);
					if (index % 2 !== 0) {
						embed.addFields([{ name: `\u200b`, value: `\u200b` }]);
					}
				});
			}

			// Daily forecast
			let daily = response.daily as unknown as DailyWeather[];
			if (daily?.length) {
				embed.addFields([
					{
						name: `Forecast: 7 Days`,
						value: daily
							.map(
								(d) =>
									`**${DateTime.fromJSDate(d.dt).toLocaleString(DateTime.DATE_SHORT)}**: 	${weatherIconCodes.get(
										d.weather.conditionId
									)} (${d.weather.description}) Low ${d.weather.temp.min}°F / High ${d.weather.temp.max}°F`
							)
							.join('\n')
					}
				]);
			} else {
				embed.addFields([{ name: `Forecast: 7 Days`, value: 'Unavailable right now.' }]);
			}

			// Update message in an async function
			(async () => {
				try {
					let guild = await this.container.client.guilds.fetch(weather.guild);
					if (guild instanceof Guild) {
						let channel = await guild.channels.fetch(weather.channel);
						if (channel instanceof TextChannel || channel instanceof NewsChannel) {
							let message = await channel.messages.fetch(weather.message);
							if (message instanceof Message) {
								await message.edit({ embeds: [embed] });
								this.container.logger.info(`${gray(`prepareWeather CRON:`)} ${magentaBright(`${weather.name} ${green(`DONE`)}.`)}`);
							} else {
								this.container.client.logger.error(
									`${gray(`prepareWeather CRON:`)} ${weather.message} did not resolve to a valid Message!`
								);
							}
						} else {
							this.container.client.logger.error(`weatherSettings.channel did not resolve to a TextChannel or NewsChannel!`);
						}
					} else {
						this.container.client.logger.error(`weatherSettings.guild did not resolve to a Guild!`);
					}
				} catch (e) {
					this.container.client.logger.error(e);
				}
			})();
		} catch (error) {
			this.container.client.logger.error(error);
		} finally {
			setTimeout(() => this.queue.shift(), 15000);
		}
	}
}
