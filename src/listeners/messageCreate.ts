import { ApplyOptions } from '@sapphire/decorators';
import { Listener, ListenerOptions } from '@sapphire/framework';
import { gray, green, yellow } from 'colorette';

import type { Message } from 'discord.js';
import { AsyncQueue } from '@sapphire/async-queue';
import { CrosspostSettings } from '../config';
@ApplyOptions<ListenerOptions>({
	event: 'messageCreate'
})
export class MessageCreateEvent extends Listener {
	public run(message: Message) {
		this.crosspostIfNewsChannel(message);
	}

	private static crosspostQueue = new AsyncQueue();

	private async processCrosspost(message: Message) {
		await MessageCreateEvent.crosspostQueue.wait();
		try {
			await message.crosspost();
			this.container.logger.info(
				`${gray(`crosspostIfNewsChannel:`)} Message ${yellow(message.id)} ${green(`crossposted`)}. ${yellow(
					MessageCreateEvent.crosspostQueue.remaining - 1
				)} messages in queue.`
			);
		} catch (error) {
			this.container.logger.error(error);
		} finally {
			// Wait 15 seconds before publishing the next message
			setTimeout(() => MessageCreateEvent.crosspostQueue.shift(), 15000);
		}
	}

	/**
	 * Crosspost this message if it was published in a news channel. Requires MANAGE_MESSAGES on client!
	 */
	private crosspostIfNewsChannel(message: Message) {
		if (
			message.crosspostable &&
			!message.cleanContent.startsWith('This message contains current weather, forecast, and alerts') &&
			CrosspostSettings.ignoreChannels.indexOf(message.channel.id) === -1
		) {
			this.processCrosspost(message);
			this.container.logger.info(`${gray(`crosspostIfNewsChannel:`)} Message ${yellow(message.id)} queued for crossposting.`);
		}
	}
}
