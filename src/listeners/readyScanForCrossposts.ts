import { ApplyOptions } from '@sapphire/decorators';
import { Listener, ListenerOptions } from '@sapphire/framework';
import { gray, green, magenta, magentaBright, yellow } from 'colorette';

import { AsyncQueue } from '@sapphire/async-queue';
import type { NewsChannel, Message } from 'discord.js';
import { DateTime } from 'luxon';
import { DiscordSnowflake } from '@sapphire/snowflake';
import { CrosspostSettings } from '../config';

@ApplyOptions<ListenerOptions>({
	once: true,
	event: 'ready'
})
export class ReadyScanForCrosspostsEvent extends Listener {
	private channelQueue = new AsyncQueue();
	private crosspostQueue = new AsyncQueue();

	/**
	 * Run the listener
	 */
	public run() {
		this.container.logger.info(`${gray(`ReadyScanForCrosspostsEvent:`)} Scanning news channels for posts that need crossposted...`);
		this.scan();
	}

	/**
	 * Scans every guild for every GUILD_NEWS channel and adds it to the queue for cross-post processing.
	 */
	private async scan() {
		this.container.client.guilds.cache.each(async (guild) => {
			guild.channels.cache.each(async (channel) => {
				if (channel.type !== "GUILD_NEWS" || CrosspostSettings.ignoreChannels.indexOf(channel.id) !== -1) return;
				this.processChannel(channel);
			});
		});
	}

	/**
	 * Add a channel into the queue to scan for messages to crosspost.
	 * @param {NewsChannel} channel
	 */
	private async processChannel(channel: NewsChannel) {
		// Wait for queue to be ready
		await this.channelQueue.wait();
		this.container.logger.info(`${gray(`ReadyScanForCrosspostsEvent:`)} Scanning ${magenta(`${channel.guild.name} -> ${channel.name}`)}.`);

		try {
			// Generate epoch 1 day previously; we do not want to publish messages more than 1 days old.
			let epochTimestamp = DateTime.now().minus({ days: 1 }).toJSDate();
			let epoch = `${DiscordSnowflake.generate({ timestamp: epochTimestamp })}`;

			// Fetch messages
			let messages = await channel.messages.fetch({ after: epoch });

			if (messages) {
				// We only want messages we can crosspost.
				let crosspostMessages = messages.filter(
					(message) =>
						message.crosspostable &&
						!message.cleanContent.startsWith('This message contains current weather, forecast, and alerts') &&
						CrosspostSettings.ignoreMessages.indexOf(message.id) === -1
				);

				// Add each message to the crosspost queue.
				crosspostMessages.each((message) => this.crosspost(message));

				this.container.logger.info(
					`${gray(`ReadyScanForCrosspostsEvent:`)} ${magenta(`${channel.guild.name} -> ${channel.name}`)} queued ${yellow(
						crosspostMessages.size
					)} messages for crossposting. ${yellow(this.channelQueue.remaining - 1)} channels in queue.`
				);
			}
		} catch (error) {
			this.container.logger.error(error);
		} finally {
			// Wait 5 seconds before allowing the next item in the queue to execute.
			setTimeout(() => this.channelQueue.shift(), 5000);
		}
	}

	/**
	 * Add a message into the queue for crossposting.
	 * @param {Message} message
	 */
	private async crosspost(message: Message) {
		// Wait for the queue to be ready.
		await this.crosspostQueue.wait();
		try {
			// Crosspost the message
			await message.crosspost();
			this.container.logger.info(
				`${gray(`ReadyScanForCrosspostsEvent:`)} message ${magentaBright(message.id)} ${green(`crossposted`)}. ${yellow(
					this.crosspostQueue.remaining - 1
				)} messages in queue.`
			);
		} catch (error) {
			this.container.logger.error(error);
		} finally {
			// Wait 5 seconds before allowing the next message in queue to crosspost.
			setTimeout(() => this.crosspostQueue.shift(), 5000);
		}
	}
}
