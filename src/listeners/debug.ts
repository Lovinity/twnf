import { ApplyOptions } from '@sapphire/decorators';
import { Listener, ListenerOptions } from '@sapphire/framework';

@ApplyOptions<ListenerOptions>({
	event: 'debug'
})
export class UserEvent extends Listener {
	public run(info: string) {
		this.container.logger.info(info);
	}
}
