import { ApplyOptions } from '@sapphire/decorators';
import { Listener, ListenerOptions } from '@sapphire/framework';
import { gray, green, magentaBright, yellow, redBright } from 'colorette';

import * as cron from 'node-cron';
import { AsyncQueue } from '@sapphire/async-queue';
import { trafficSettings } from '../config';
import type { TrafficSource, TrafficDbRecord, TrafficDbRecords } from '../lib/types';
import { Guild, TextChannel, NewsChannel, Message } from 'discord.js';

import tt = require('@tomtom-international/web-sdk-services/dist/services-node.min.js');
import { JsonDB, Config } from 'node-json-db';
import { trafficIcons } from '../lib/constants';
import { Duration } from 'luxon';

@ApplyOptions<ListenerOptions>({
	once: true,
	event: 'ready'
})
export class readyTraffic extends Listener {
	private queue = new AsyncQueue();
	private messageQueue = new AsyncQueue();
	private db = new JsonDB(new Config('traffic', true, false, '/'));

	private activeIDs: string[] = [];
	private cleanup: number = 0;

	public run() {
		this.container.logger.info(`${gray(`ReadyTrafficEvent:`)} Preparing...`);

		// Schedule a "cron job" to run the weather operations
		cron.schedule(trafficSettings.cronTab, () => {
			this.container.logger.info(`${gray(`ReadyTrafficEvent CRON:`)} Starting.`);

			this.activeIDs = [];

			this.cleanup = 0;

			// Queue each weather operation from the config
			trafficSettings.traffic.forEach((traffic) => {
				this.runTraffic(traffic);
			});

			this.queueCleanup();

			this.container.logger.info(`${gray(`ReadyTrafficEvent CRON:`)} ${green(`queued`)}`);
		});

		this.container.logger.info(`${gray(`ReadyTrafficEvent:`)} ${green(`READY`)}`);
	}

	/**
	 * Add a traffic operation to the queue.
	 * @param {TrafficSource} traffic A TrafficSource object from the config file
	 */
	private async runTraffic(traffic: TrafficSource) {
		// Wait for the queue to be ready to process the next operation.
		await this.queue.wait();
		this.container.logger.info(`${gray(`ReadyTrafficEvent.runTraffic:`)} ${magentaBright(`Running TomTom on ${traffic.name}`)}.`);
		try {
			// Get the traffic data from TomTom
			let trafficData = await tt.services.incidentDetailsV5({
				key: trafficSettings.TomTom.apiKey,
				boundingBox: traffic.bbox,
				fields: '{incidents{properties{id,iconCategory,magnitudeOfDelay,events{description,code},startTime,endTime,from,to,length,delay,roadNumbers,aci{probabilityOfOccurrence,numberOfReports,lastReportTime}}}}'
			});

			if (trafficData && trafficData.incidents) {
				this.cleanup++;

				for (let incident of trafficData.incidents) {
					// NOTE: IDE is not intelligent enough to detect Array.filter to weed out undefined properties. Do not use that.
					if (!incident.properties || !incident.properties.id) continue;

					// Determine traffic delay (we only want proficiency of minutes)
					let delayDuration: Duration | undefined;
					if (incident.properties.delay) {
						delayDuration = Duration.fromObject({ minutes: Math.ceil(incident.properties.delay / 60)});

						// Skip traffic incidents with an estimated delay under 5 minutes; helps prevent Discord rate limiting for trivial delays
						if (delayDuration.minutes < 5) {
							continue;
						}
					}
					
					// Add this to active traffic incidents; by this point, we consider it valid.
					this.activeIDs.push(incident.properties.id);

					// Build Discord message string
					let message: string = `${trafficIcons.magnitudeOfDelay[incident.properties.magnitudeOfDelay || 0]} __From **${
						incident.properties.from || 'Unknown Road'
					}** to **${incident.properties.to || 'Unknown Road'}**__ (${incident.properties.id})
> 🚧 Events: **${
						incident.properties.events
							? incident.properties.events.map((event) => event.description || `Unknown Event`).join(', ')
							: `Unknown`
					}**
${typeof delayDuration !== 'undefined' ? `> ⏳ Approximate Travel Delay (minutes): **${delayDuration.minutes}**` : ``}
➖`;

					// Queue operation
					this.processTraffic(`Incident ${incident.properties.id}`, async () => {
						if (!incident.properties || !incident.properties.id) return;

						let dbRecord: TrafficDbRecord | undefined;
						try {
							dbRecord = await this.db.getObject<TrafficDbRecord>(`/${incident.properties.id}`);
							if (!dbRecord) {
								throw new Error(`Record does not exist`);
							}
						} catch {
							// Record did not exist. Create it along with a Discord message
							this.container.logger.info(
								`${gray(`ReadyTrafficEvent.processTraffic.process:`)} ${magentaBright(
									`Incident ${incident.properties.id}`
								)}: Creating a Discord message.`
							);
							let guild = await this.container.client.guilds.fetch(traffic.guild);
							if (guild instanceof Guild) {
								let channel = await guild.channels.fetch(traffic.channel);
								if (channel instanceof TextChannel || channel instanceof NewsChannel) {
									let dcMessage = await (channel as TextChannel).send(message);
									if (!dcMessage) throw new Error(`Error sending incidents message.`);

									this.db.push(`/${incident.properties.id}`, {
										discordMessageID: dcMessage.id,
										discordChannelID: traffic.channel,
										discordGuildID: traffic.guild,
										discordMessage: message
									} as TrafficDbRecord);
								}
							}
						}

						// Message changed; we should update it
						if (dbRecord && message !== dbRecord.discordMessage) {
							this.container.logger.info(
								`${gray(`ReadyTrafficEvent.processTraffic.process:`)} ${magentaBright(
									`Incident ${incident.properties.id}`
								)}: Updating Discord message.`
							);
							let guild = await this.container.client.guilds.fetch(traffic.guild);
							if (guild instanceof Guild) {
								let channel = await guild.channels.fetch(traffic.channel);
								if (channel instanceof TextChannel || channel instanceof NewsChannel) {
									let dcMessage = await (channel as TextChannel).messages.fetch(dbRecord.discordMessageID);
									if (dcMessage instanceof Message) {
										await dcMessage.edit(message);

										this.db.push(`/${incident.properties.id}`, {
											discordMessageID: dcMessage.id,
											discordChannelID: traffic.channel,
											discordGuildID: traffic.guild,
											discordMessage: message
										} as TrafficDbRecord);
									}
								}
							}
						} else {
							this.container.logger.info(
								`${gray(`ReadyTrafficEvent.processTraffic.process:`)} ${magentaBright(
									`Incident ${incident.properties.id}`
								)}: No changes.`
							);
						}
					});
				}
			}
		} catch (error) {
			this.container.client.logger.error(error);
		} finally {
			this.container.logger.info(
				`${gray(`ReadyTrafficEvent.runTraffic:`)} ${magentaBright(`${traffic.name}`)} ${green(`DONE`)}. ${yellow(
					this.queue.remaining - 1
				)} TomTom operations in queue.`
			);
			setTimeout(() => this.queue.shift(), 15000);
		}
	}

	private async processTraffic(name: string, fn: () => Promise<void>) {
		// Wait for the queue to be ready to process the next operation.
		await this.messageQueue.wait();
		this.container.logger.info(`${gray(`ReadyTrafficEvent.processTraffic:`)} Running operation ${magentaBright(name)}.`);
		try {
			await fn();
		} catch (error) {
			this.container.client.logger.error(error);
		} finally {
			this.container.logger.info(
				`${gray(`ReadyTrafficEvent.processTraffic:`)} Operation ${magentaBright(name)} ${green(`DONE`)}. ${yellow(
					this.messageQueue.remaining - 1
				)} message operations in queue.`
			);
			setTimeout(() => this.messageQueue.shift(), 3000);
		}
	}

	/**
	 * Queue a cleanup procedure to remove messages / db records whose incidents no longer exist.
	 */
	private async queueCleanup() {
		// Queue delete / cleanup once all traffic items have been processed
		await this.queue.wait();

		if (this.cleanup < trafficSettings.traffic.length) {
			this.container.logger.warn(
				`${gray(
					`ReadyTrafficEvent.queueCleanup:`
				)} One or more traffic sources malfunctioned / did not return data. Cleanup was skipped for now.`
			);

			this.queue.shift();
			return;
		}

		this.container.logger.info(`${gray(`ReadyTrafficEvent.queueCleanup:`)} ${yellow(`started`)}.`);

		// Queue cleanup operations
		let dbRecords = await this.db.getObject<TrafficDbRecords>('/');
		for (let key in dbRecords) {
			if (!Object.prototype.hasOwnProperty.call(dbRecords, key) || this.activeIDs.indexOf(key) === -1) {
				this.container.logger.info(
					`${gray(`ReadyTrafficEvent.queueCleanup:`)} ${magentaBright(key)} no longer active and queued for deletion.`
				);
				this.processTraffic(`Delete: ${key}`, async () => {
					// Since we are deleting the record first, we have to make a copy of the snowflake.
					let dcMessageID = dbRecords[key].discordMessageID;
					let dcChannelID = dbRecords[key].discordChannelID;
					let dcGuildID = dbRecords[key].discordGuildID;

					this.db.delete(`/${key}`);
					this.container.logger.info(
						`${gray(`ReadyTrafficEvent.processTraffic.cleanup:`)} ${redBright(`DELETED`)} db entry ${magentaBright(key)}.`
					);

					let guild = await this.container.client.guilds.fetch(dcGuildID);
					if (guild instanceof Guild) {
						let channel = await guild.channels.fetch(dcChannelID);
						if (channel instanceof TextChannel || channel instanceof NewsChannel) {
							let dcMessage = await (channel as TextChannel).messages.fetch(dcMessageID);
							if (dcMessage instanceof Message) {
								await dcMessage.delete();
								this.container.logger.info(
									`${gray(`ReadyTrafficEvent.processTraffic.cleanup:`)} ${redBright(
										`DELETED`
									)} discord message for db entry ${magentaBright(key)}.`
								);
							}
						}
					}
				});
			}
		}

		this.container.logger.info(`${gray(`ReadyTrafficEvent.queueCleanup:`)} ${green(`PROCESSED`)}.`);
		this.queue.shift();
	}
}
