import type { HexColorString } from 'discord.js';
import { join } from 'path';

export const rootDir = join(__dirname, '..', '..');
export const srcDir = join(rootDir, 'src');

export const RandomLoadingMessage = ['Computing...', 'Thinking...', 'Cooking some food', 'Give me a moment', 'Loading...'];

/*
    weather
*/

export const weatherIconCodes = new Map<number, string>([
	[200, '⚡🌦️'],
	[201, '⚡🌧️'],
	[202, '⚡☔'],
	[210, '🌩️'],
	[211, '⚡'],
	[212, '⚡⚡'],
	[221, '⚡⚡'],
	[230, '⚡🌦️'],
	[231, '⚡🌦️'],
	[232, '⚡🌧️'],

	[300, '🌦️'],
	[301, '🌦️'],
	[302, '🌧️'],
	[310, '🌦️'],
	[311, '🌧️'],
	[312, '☔'],
	[313, '🌧️'],
	[314, '☔'],
	[321, '🌦️'],

	[500, '🌦️'],
	[501, '🌧️'],
	[502, '☔'],
	[503, '☔☔'],
	[504, '☔☔☔'],
	[511, '🧊🌧️'],
	[520, '🌦️'],
	[521, '🌧️'],
	[522, '☔'],
	[531, '☔'],

	[600, '🌨️'],
	[601, '❄️'],
	[602, '☃️'],
	[611, '🧊❄️'],
	[612, '🧊🌨️'],
	[613, '🧊❄️'],
	[615, '🌦️🌨️'],
	[616, '🌧️❄️'],
	[620, '🌨️'],
	[621, '❄️'],
	[622, '☃️'],

	[701, '☁️'],
	[711, '💨'],
	[721, '🌁'],
	[731, '💨'],
	[741, '🌫️'],
	[751, '💨'],
	[761, '💨'],
	[762, '🌋'],
	[771, '⚡⚡⚡'],
	[781, '🌪️'],

	[800, '☀️'],
	[801, '🌤️'],
	[802, '⛅'],
	[803, '🌥️'],
	[804, '☁️']
]);

export const weatherIcons = {
	conditions: '⛅',
	cloudCover: '☁️',
	rain: '☔',
	snow: '❄️',
	temperature: '🌡️',
	wind: '🌬️',
	humidity: '😓',
	pressure: '📈',
	visibility: '👁️',
	uvIndex: '😎',
	epaIndex: ''
};

export const weatherAlertSeverityIcons = new Map<string, string>([
	['Minor', '🔵'],
	['Moderate', '🟡'],
	['Severe', '🟠'],
	['Extreme', '🔴']
]);

export const weatherAlertSeverityColors = new Map<string, HexColorString>([
	['Minor', '#0000ff'],
	['Moderate', '#ffff00'],
	['Severe', '#ff7800'],
	['Extreme', '#ff0000']
]);

export const weatherAlertSeverityExplanation = new Map<string, string>([
	['Minor', 'Minimal threat to life or property'],
	['Moderate', 'Possible threat to life or property'],
	['Severe', 'Significant threat to life or property'],
	['Extreme', 'Extraordinary threat to life or property']
]);

export const weatherAlertCertaintyExplanation = new Map<string, string>([
	['Observed', 'Ongoing now or already occurred'],
	['Likely', '> 50% chance of happening'],
	['Possible', '<= 50% chance of happening'],
	['Unlikely', 'Not expected to occur']
]);

export const weatherAlertUrgencyExplanation = new Map<string, string>([
	['Immediate', 'Response action should be taken immediately!'],
	['Expected', 'Response action should be taken soon (next hour)'],
	['Future', 'Response action should be taken in the near future'],
	['Past', 'Response action no longer required']
]);

export const weatherAlertResponseTypeExplanation = new Map<string, string>([
	['Shelter', 'Take shelter in place or as instructed'],
	['Evacuate', 'Relocate as instructed'],
	['Prepare', 'Make preparations as instructed'],
	['Execute', 'Execute a pre-planned activity as instructed'],
	['Avoid', 'Avoid the subject event as per the instruction'],
	['Monitor', 'Attend to information sources as instructed'],
	['Assess', 'Evaluate the information in the message'],
	['AllClear', 'The subject event no longer poses a threat or concern and any follow on action is described in instructions'],
	['None', 'No action recommended']
]);

/*
	TRAFFIC
*/

export const trafficIcons = {
	magnitudeOfDelay: {
		0: '❓',
		1: 'ℹ️',
		2: '⚠️',
		3: '‼️',
		4: '⛔'
	}
};
