import type { Snowflake } from 'discord.js';

export type HEX = `#${string}`;
export type BBOX = `${number},${number},${number},${number}`;

/*
    weather
*/
export interface WeatherSource {
	key: string;
	name: string;
	color: HEX;
	lat: number;
	long: number;
	guild: Snowflake;
	channel: Snowflake;
	message: Snowflake;
}
export interface WeatherSettingsConfig {
	openWeatherMap: {
		apiKey: string;
		units: 'imperial' | 'metric';
	};
	cronTab: string;
	weather: WeatherSource[];
}
export interface WeatherAlertDbRecords {
	[key: string]: WeatherAlertDbRecord;
}
export interface WeatherAlertDbRecord {
	discordMessageID: Snowflake;
	discordChannelID: Snowflake;
	discordGuildID: Snowflake;
	discordMessage: string;
	alert: WeatherAlertDbRecordAlert;
}

export interface WeatherAlertDbRecordAlert {
	event: string;
	headline: string | null;
	description: string;
	instruction: string | null;
	effective: string;
	expires: string;
	onset: string | null;
	ends: string | null;
	urgency: string;
	severity: string;
	certainty: string;
	response: string;
	tornadoDamageThreat?: string;
	tornadoDetection?: string;
	windGust?: number;
	hailSize?: number;
	eventEndingTime?: string;
}

// weathered does not have the correct type definitions
export type AlertsResponseModified = {
	features: AlertsFeatureModified[];
};

// weathered does not have the correct type definitions
export type AlertsFeatureModified = {
	id: string;
	type: string;
	properties: {
		id: string;
		areaDesc: string;
		geocode: {
			UGC: string[];
			SAME: string[];
		};
		affectedZones: string[];
		references: [
			{
				'@id': string;
				identifier: string;
				sender: string;
				sent: string;
			}
		];
		sent: string;
		effective: string;
		onset: string | null;
		expires: string;
		ends: string | null;
		status: 'Actual' | 'Exercise' | 'System' | 'Test' | 'Draft';
		messageType: 'Alert' | 'Update' | 'Cancel' | 'Ack' | 'Error';
		category: 'Met' | 'Geo' | 'Safety' | 'Security' | 'Rescue' | 'Fire' | 'Health' | 'Env' | 'Transport' | 'Infra' | 'CBRNE' | 'Other';
		severity: 'Extreme' | 'Severe' | 'Moderate' | 'Minor' | 'Unknown';
		certainty: 'Observed' | 'Likely' | 'Possible' | 'Unlikely' | 'Unknown';
		urgency: 'Immediate' | 'Expected' | 'Future' | 'Past' | 'Unknown';
		event: string;
		sender: string;
		senderName: string;
		headline: string | null;
		description: string;
		instruction: string | null;
		response: 'Shelter' | 'Evacuate' | 'Prepare' | 'Execute' | 'Avoid' | 'Monitor' | 'Assess' | 'AllClear' | 'None';
		parameters: {
			[key: string]: any[];
		};
	};
};

/*
	TRAFFIC
*/

export interface TrafficSource {
	key: string;
	name: string;
	color: HEX;
	guild: Snowflake;
	channel: Snowflake;
	bbox: BBOX;
	language?: string;
}

export interface TrafficSettingsConfig {
	TomTom: {
		apiKey: string;
	};
	cronTab: string;
	traffic: TrafficSource[];
}

export interface TrafficDbRecords {
	[key: string]: TrafficDbRecord;
}

export interface TrafficDbRecord {
	discordMessageID: Snowflake;
	discordChannelID: Snowflake;
	discordGuildID: Snowflake;
	discordMessage: string;
}

/*
	CROSSPOST
*/

export interface CrosspostSettingsConfig {
	ignoreChannels: Snowflake[];
	ignoreMessages: Snowflake[];
}
